package com.example.emi.quiz;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class Listactivity extends AppCompatActivity {

    RecyclerView recycle;
    Context ctx;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listactivity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        recycle=(RecyclerView) findViewById(R.id.recycle);

        String[] names= new String[5];
        names[0]="ahmed";
        names[1]="achour";

        names[2]="boughnim";
        names[3]="salah";
        names[4]="fattoum";


        recycle.setLayoutManager(new LinearLayoutManager(ctx));
        Adapternames adapter= new Adapternames(this,names);
        recycle.setAdapter(adapter);





        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

}
