package com.example.emi.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Loginactivity extends AppCompatActivity {

EditText txtuser, txtpwd;
Button btnlogin;
String user, pwd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginactivity);



         txtuser=(EditText) findViewById(R.id.txtuser);
        txtpwd=(EditText)findViewById(R.id.txtpwd);
        btnlogin=(Button) findViewById(R.id.btnlogin);


        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user=txtuser.getText().toString();
                pwd=txtpwd.getText().toString();

                if((user!=null && user.equals("admin") && (pwd!=null && pwd.equals("admin")))){

                    Intent intent = new Intent(Loginactivity.this, Quizactivity.class);
                    startActivity(intent);
                    finish();


                }
                else{
                     txtuser.setText(null);
                     txtpwd.setText(null);
                       Snackbar.make(view, "Veuillez verifier vos coordonnees!", Snackbar.LENGTH_LONG)
                          .setAction("Action", null).show();
            }}
        });

    }

}
